# Formen für ein CAD-Programm

**Voraussetzung**:
Für die letzte Teilaufgabe wird die Vorlesung zum Thema Ausnahmebehandlung (Exception Handling) vorausgesetzt.

**Ziel**:
Wiederholung wichtiger Konzepte aus dem „Praktikum Programmieren“ wie Spezifikationsvererbung, dynamisches Binden, Casting.

**Dauer**:
< 1 Stunde

## Aufgabenstellung
Für ein CAD-Programm sollen grundlegende Klassen für zweidimensionale Formen (engl. *Shape*) programmiert werden.

### (a) Rechteck und Kreis implementieren
Implementieren Sie eine Klasse, die ein Rechteck beschreibt (engl. *Rectangle*), und eine Klasse, die einen Kreis beschreibt (engl. *Circle*). Beide Klassen sollen geeignete Konstruktoren, Felder und Methoden enthalten. Diese Klassen implementieren das Interface `Shape`:

```java
package io.fp.shapes;

public interface Shape {
    // Die Methode area() gibt den Flächeninhalt der Form zurück
    double area();
    
    // Die Methode circumference() gibt den Umfang der Form zurück
    double circumference();
}
```

Schreiben Sie eine ausführbare Klasse `App`, um die Methoden der beiden Klassen zu überprüfen.

**Hinweis:** Mathematische Funktionen und Konstanten wie π sind in der Klasse `java.lang.Math` als statische Elemente verfügbar (z.B. `Math.PI`; siehe Java-API Dokumentation).

### (b) Klasse `Util` erstellen
Schreiben Sie eine Klasse `Util`, die zwei statische Methoden enthält, welche den Gesamtflächenbedarf bzw. den Gesamtumfang aller Formen in einem Array berechnen. Die beiden Methoden haben folgende Signatur:

```java
public static double accumulateArea(Shape[] shapes) {
    // Implementierung
}

public static double accumulateCircumference(Shape[] shapes) {
    // Implementierung
}
```

Erweitern Sie die ausführbare Klasse `App` aus Teilaufgabe (a) so, dass Sie ein `Shape`-Array erstellen und es mit diversen Rechtecken und Kreisen füllen. Verwenden Sie die Methoden aus der Klasse `Util`, um die Gesamtfläche bzw. den Gesamtumfang der Objekte im Array zu berechnen.

### (c) Schnittstellen vs. Objekt-Typen
Ein C-Programmierer, der auch PHP4 toll findet, sieht Ihren Code und sagt: "Das kapiere ich nicht! Wozu verwendest Du Schnittstellen? Das geht doch auch ohne!" Er fügt der Klasse `Util` eine Methode mit folgender Signatur hinzu, die tatsächlich das Gleiche macht, ohne die Schnittstelle `Shape` zu verwenden:

```java
public static double accumulateArea(Object[] objects) {
    // Implementierung
}
```

Programmieren Sie diese Methode. Überlegen Sie, welche Nachteile diese Methode gegenüber der Variante hat, die die Schnittstelle `Shape` verwendet.

### (d) Rechteck mit zentriertem Kreis (RectangleCircle)
Erstellen Sie eine neue Form unter Verwendung der Klassen aus Teilaufgabe (a). Die neue Form ist ein Rechteck, das einen Kreis zentriert enthält (Klasse `RectangleCircle`). Zur Fläche dieser neuen Form zählt nur der Bereich, der sich zwischen den Rechteckbegrenzungen und dem Kreis befindet. Der Umfang ist durch die Summe der Umfänge von Kreis und Rechteck gegeben.

Überlegen Sie sich, welche Konstruktoren, Felder und Methoden nötig sind. Die neue Klasse muss ebenfalls die Schnittstelle `Shape` implementieren.

**Hinweis:** Versuchen Sie, so viel wie möglich aus Aufgabe (a) wiederzuverwenden!

Verändern Sie anschließend Ihre ausführbare Klasse `App` so, dass Sie auch Objekte der Klasse `RectangleCircle` im Array speichern und damit deren Flächeninhalt bzw. Umfang in die Berechnung einfließt.